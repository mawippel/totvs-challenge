FROM gradle:8.6.0-jdk17 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

# create final docker image
FROM eclipse-temurin:17-jdk
# Prep container
RUN mkdir application

COPY --from=build /home/gradle/src/build/libs/*.jar /app/application.jar

ENTRYPOINT ["java", "-XX:InitialRAMPercentage=60.0", "-XX:MaxRAMPercentage=60.0","-jar","/app/application.jar"]
