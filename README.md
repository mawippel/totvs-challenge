# TOTVS Challenge

## Introducao
API REST de Contas a Pagar, com funcionalidade de cadastro, atualização, listagem e importação de CSV para data load.

## Tech stack

- Java 17
- Spring Boot (TomCat embarcado)
- Spring Data JPA
- Spring Security
- Lombok
- JUnit 5
- PostgreSQL
- Flyway

## Estrutura do Projeto

O projeto segue uma estrutura de pacotes orientada a dominio (DDD), com dois bounded contexts: _**autenticacao**_ e _**contas**_.
Os pacotes não foram divididos entre domain e infra, todos estão abaixo do próprio domínio (_auth_ e _conta_) por questões de visualização
e para evitar uma estrutura de pacotes complexa demais para um problema pequeno.

A implementação segue a base dos principios [SOLID](https://en.wikipedia.org/wiki/SOLID) e [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).
Evitei implementar interfaces onde não era necessário, sendo utilizado somente na camada de service (evitando generalizacao prematura),
onde realmente faz sentido a utilização de interfaces a fim de aplicar o Liskov Substitution Principle.
## Modelo de dados
```sql
conta
(
    -- SERIAL ao inves de UUID por questoes de performance
    id                SERIAL PRIMARY KEY,
    descricao         VARCHAR(255) NOT NULL,
    valor             NUMERIC(30,2) NOT NULL,
    -- Enum definido na aplicacao - PENDENTE, PAGA, ATRASADA
    situacao          VARCHAR(32),
    -- Data no padrao ISO-8601 no timezone UTC
    data_vencimento   TIMESTAMP NOT NULL,
    -- Data no padrao ISO-8601 no timezone UTC
    data_pagamento    TIMESTAMP,
    created_by        VARCHAR(255)
);

api_key
(
    id           SERIAL PRIMARY KEY,
    -- email do usuario vinculado a API Key
    email        VARCHAR(255) NOT NULL,
    -- UUID gerado pela aplicacao
    token        UUID NOT NULL,
    -- Data de criacao utilizada para calculo da expiracao
    created_at   TIMESTAMP NOT NULL
);
```

## Rodando o projeto

```bash
# Entre no diretorio do projeto
cd ./totvs-challenge

# Suba o docker-compose
docker-compose up -d
```

### Autenticacao
Para suprir o requisito de autenticação, optei por implementar um método de autenticação simples, simulando o funcionamento de uma API Key.
O endpoint de criação de API Key não e protegido, possibilitando qualquer pessoa de criar uma API Key com acesso total utilizando seu email.
A API Key tem validade de 30 minutos apos a sua criação.

Apos efetuar a criacao de uma API Key, a API retornara um token com acesso a API de Contas a Pagar.

```bash
curl --location 'http://localhost:8080/api/api-keys' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "mawippel2@hotmail.com"
}'
```

Exemplo de resposta:

```json
{
    "token": "d996c2e5-55f3-4a81-814a-5aaf5858b65a"
}
```

Este token deve ser passado como um _**Bearer token**_ para a API de Contas a Pagar.

### API de Contas a Pagar

#### Criar nova Conta:
```bash
curl --location 'http://localhost:8080/api/contas' \
--header 'Content-Type: application/json' --header 'Authorization: Bearer d996c2e5-55f3-4a81-814a-5aaf5858b65a' \
--data '{
    "valor": 520.32,
    "descricao": "nice descricao",
    "situacao": "PAGA",
    "dataPagamento": "2024-02-24T00:00:00.000",
    "dataVencimento": "2024-02-26T00:00:00.000"
}'
```

#### Atualizar Conta:
```bash
curl --location --request PUT 'http://localhost:8080/api/contas/1' \
--header 'Content-Type: application/json' --header 'Authorization: Bearer d996c2e5-55f3-4a81-814a-5aaf5858b65a' \
--data '{
    "valor": 521.32,
    "descricao": "nice descricao 2",
    "situacao": "PENDENTE",
    "dataPagamento": "2024-03-24T00:00:00.000",
    "dataVencimento": "2024-04-26T00:00:00.000"
}'
```

#### Atualizar Situacao da Conta:
```bash
curl --location --request PATCH 'http://localhost:8080/api/contas/1' \
--header 'Content-Type: application/json' --header 'Authorization: Bearer d996c2e5-55f3-4a81-814a-5aaf5858b65a' \
--data '{
    "situacao": "PAGA"
}'
```

#### Retornar Conta pelo ID:
```bash
curl --location 'http://localhost:8080/api/contas/1' --header 'Authorization: Bearer d996c2e5-55f3-4a81-814a-5aaf5858b65a'
```

#### Listar Contas (Filtros e Paginacao):
```bash
curl --location 'http://localhost:8080/api/contas?descricao=nice&data_vencimento=2024-04-27T00%3A00%3A00&page_size=1&page_number=1' --header 'Authorization: Bearer d996c2e5-55f3-4a81-814a-5aaf5858b65a'
```

#### Valor Total pago no periodo:
Foi utilizado o padrao de [Custom Method](https://cloud.google.com/apis/design/custom_methods) do Google API Designs.
```bash
curl --location 'http://localhost:8080/api/contas/totais:noPeriodo?data_inicio=2024-01-20T00%3A00%3A00&data_fim=2024-04-27T00%3A00%3A00' --header 'Authorization: Bearer d996c2e5-55f3-4a81-814a-5aaf5858b65a'
```

#### Importacao de Contas de um arquivo CSV:
```bash
curl --location 'http://localhost:8080/api/contas/uploads' \
--header 'Authorization: Bearer d996c2e5-55f3-4a81-814a-5aaf5858b65a' \
--form 'file=@"/dev/totvs-challenge/src/main/resources/example/sample.csv"'
```

# Test Coverage

Os pacotes de service e controller possuem acima de 80% de line coverage:
![coverage.png](src%2Fmain%2Fresources%2Fimgs%2Fcoverage.png)

# Postman Collection
Fiz o upload da collection do Postman no repositorio para facilitar a execucao dos endpoints caso necessario.
A collection se encontra no diretorio de resources da aplicacao.

# Apresentacao

O envio do video foi feito atraves do Loom na plataforma Coodesh.