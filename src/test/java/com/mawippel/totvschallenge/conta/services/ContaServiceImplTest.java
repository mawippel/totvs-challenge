package com.mawippel.totvschallenge.conta.services;

import com.mawippel.totvschallenge.domains.conta.entities.Conta;
import com.mawippel.totvschallenge.domains.conta.exceptions.ContaNotFoundException;
import com.mawippel.totvschallenge.domains.conta.repositories.ContaRepository;
import com.mawippel.totvschallenge.domains.conta.services.ContaServiceImpl;
import com.mawippel.totvschallenge.domains.conta.shared.enums.SituacaoConta;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ContaServiceImplTest {

    @Mock
    private ContaRepository contaRepository;

    @Captor
    ArgumentCaptor<Conta> contaArgumentCaptor;

    @InjectMocks
    private ContaServiceImpl contaService;

    @Test
    void updateConta_ExistingConta_ReturnsUpdatedConta() {
        // Arrange
        Long id = 123L;
        Conta contaToUpdate = Conta.builder()
                .valor(new BigDecimal(123))
                .descricao("nova descricao")
                .situacao(SituacaoConta.PAGA)
                .dataVencimento(LocalDateTime.now())
                .dataPagamento(LocalDateTime.now())
                .build();
        Conta contaFromDb = Conta.builder()
                .id(id)
                .valor(new BigDecimal(123))
                .descricao("foobar")
                .situacao(SituacaoConta.PAGA)
                .dataVencimento(LocalDateTime.now())
                .dataPagamento(LocalDateTime.now())
                .build();

        when(contaRepository.findById(anyLong())).thenReturn(Optional.of(contaFromDb));

        // Act
        Conta updatedConta = contaService.updateConta(id, contaToUpdate);
        verify(contaRepository).saveAndFlush(contaArgumentCaptor.capture());

        Conta value = contaArgumentCaptor.getValue();

        // Assert
        assertEquals(contaToUpdate.getDescricao(), value.getDescricao());
        // Add more assertions as needed
    }

    @Test
    void updateConta_NonExistingConta_ThrowsContaNotFoundException() {
        Long id = 1L;
        Conta contaToUpdate = new Conta();
        when(contaRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ContaNotFoundException.class, () -> contaService.updateConta(id, contaToUpdate));
    }

    @Test
    void updateSituacaoConta_ValidContaAndPagaSituacao_UpdatesContaAndReturnsUpdatedConta() {
        // Arrange
        Long id = 1L;
        Conta contaFromDb = new Conta();
        when(contaRepository.findById(id)).thenReturn(Optional.of(contaFromDb));
        SituacaoConta situacaoConta = SituacaoConta.PAGA;

        // Act
        contaService.updateSituacaoConta(id, situacaoConta);
        verify(contaRepository).saveAndFlush(contaArgumentCaptor.capture());

        Conta value = contaArgumentCaptor.getValue();

        // Assert
        assertEquals(situacaoConta, value.getSituacao());
    }

    @Test
    void updateSituacaoConta_NullSituacao_ThrowsResponseStatusException() {
        // Arrange
        Long id = 1L;
        SituacaoConta situacaoConta = null;

        // Act and Assert
        assertThrows(ResponseStatusException.class, () -> contaService.updateSituacaoConta(id, situacaoConta));
    }

    @Test
    void createConta_ValidConta_ReturnsSavedConta() {
        // Arrange
        Conta contaToSave = new Conta();
        Conta savedConta = new Conta();
        when(contaRepository.save(any(Conta.class))).thenReturn(savedConta);

        // Act
        Conta createdConta = contaService.createConta(contaToSave);

        // Assert
        assertNotNull(createdConta);
        assertEquals(savedConta, createdConta);
        verify(contaRepository, times(1)).save(any());
    }

    @Test
    void getContaById_ExistingId_ReturnsConta() {
        // Arrange
        Long id = 1L;
        Conta contaFromDb = new Conta();
        when(contaRepository.findById(id)).thenReturn(Optional.of(contaFromDb));

        // Act
        Conta retrievedConta = contaService.getContaById(id);

        // Assert
        assertNotNull(retrievedConta);
        assertEquals(contaFromDb, retrievedConta);
    }

    @Test
    void getContaById_NonExistingId_ThrowsContaNotFoundException() {
        // Arrange
        Long id = 1L;
        when(contaRepository.findById(id)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(ContaNotFoundException.class, () -> contaService.getContaById(id));
    }

    @Test
    void listContas_WithParameters_ReturnsListOfContas() {
        // Arrange
        String descricao = "Some Description";
        LocalDateTime dataVencimento = LocalDateTime.now();
        int pageNumber = 0;
        int pageSize = 10;
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by("id").ascending());
        List<Conta> expectedContas = Collections.singletonList(new Conta());
        when(contaRepository.getContasByDescricaoContainingIgnoreCaseAndDataVencimentoIsBefore(eq(descricao), eq(dataVencimento), eq(pageable)))
                .thenReturn(expectedContas.stream());

        // Act
        List<Conta> result = contaService.listContas(descricao, dataVencimento, pageNumber, pageSize);

        // Assert
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(expectedContas.size(), result.size());
        assertEquals(expectedContas.get(0), result.get(0));
    }

    @Test
    void getTotalValueInPeriod_ReturnsTotalValue() {
        // Arrange
        LocalDateTime dataInicio = LocalDateTime.now().minusDays(30);
        LocalDateTime dataFim = LocalDateTime.now();
        BigDecimal expectedTotalValue = BigDecimal.valueOf(1000);
        when(contaRepository.getTotalValueBetweenDataInicioAndDataFim(eq(dataInicio), eq(dataFim)))
                .thenReturn(expectedTotalValue);

        // Act
        BigDecimal result = contaService.getTotalValueInPeriod(dataInicio, dataFim);

        // Assert
        assertNotNull(result);
        assertEquals(expectedTotalValue, result);
    }

    @Test
    void importFromCSV_ValidFile_ReturnsTotalCount() {
        // Arrange
        List<String> lines = new ArrayList<>();
        lines.add("VALOR,DESCRICAO,SITUACAO,DATA_PAGAMENTO,DATA_VENCIMENTO");
        lines.add("100.00,Description,PENDENTE,2024-02-18T10:00:00,2024-02-28T10:00:00");
        lines.add("200.00,Another Description,PAGA,2024-01-18T10:00:00,2024-01-28T10:00:00");
        String csvContent = String.join("\n", lines);

        MockMultipartFile file = new MockMultipartFile("file", "sample.csv",
                "text/csv", csvContent.getBytes());
        when(contaRepository.saveAll(anyList())).thenReturn(new ArrayList<>());

        // Act
        Integer totalCount = contaService.importFromCSV(file);

        // Assert
        assertNotNull(totalCount);
        assertEquals(2, totalCount);
    }

}

