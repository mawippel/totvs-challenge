package com.mawippel.totvschallenge.conta.controllers;

import com.mawippel.totvschallenge.domains.conta.controllers.ContaController;
import com.mawippel.totvschallenge.domains.conta.entities.Conta;
import com.mawippel.totvschallenge.domains.conta.services.ContaService;
import com.mawippel.totvschallenge.domains.conta.shared.enums.SituacaoConta;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContaControllerTests {

    @Mock
    private ContaService contaService;

    @InjectMocks
    private ContaController contaController;

    @Test
    void createConta_ValidConta_ReturnsCreatedConta() {
        // Arrange
        Conta conta = new Conta();
        when(contaService.createConta(any(Conta.class))).thenReturn(conta);

        // Act
        ResponseEntity<Conta> responseEntity = contaController.createConta(conta);

        // Assert
        verify(contaService, times(1)).createConta(any());
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(conta, responseEntity.getBody());
    }

    @Test
    void updateConta_ExistingId_ReturnsUpdatedConta() {
        // Arrange
        Long id = 1L;
        Conta conta = new Conta();
        when(contaService.updateConta(eq(id), any(Conta.class))).thenReturn(conta);

        // Act
        ResponseEntity<Conta> responseEntity = contaController.updateConta(id, conta);

        // Assert
        verify(contaService, times(1)).updateConta(anyLong(), any());
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(conta, responseEntity.getBody());
    }

    @Test
    void updateSituacaoConta_ExistingId_ReturnsUpdatedConta() {
        // Arrange
        Long id = 1L;
        Conta conta = new Conta();
        when(contaService.updateSituacaoConta(eq(id), any())).thenReturn(conta);

        // Act
        ResponseEntity<Conta> responseEntity = contaController.updateSituacaoConta(id, conta);

        // Assert
        verify(contaService, times(1)).updateSituacaoConta(anyLong(), any(SituacaoConta.class));
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(conta, responseEntity.getBody());
    }

    @Test
    void getContaById_ExistingId_ReturnsConta() {
        // Arrange
        Long id = 1L;
        Conta conta = new Conta();
        when(contaService.getContaById(eq(id))).thenReturn(conta);

        // Act
        ResponseEntity<Conta> responseEntity = contaController.getContaById(id);

        // Assert
        verify(contaService, times(1)).getContaById(anyLong());
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(conta, responseEntity.getBody());
    }

    @Test
    void listContas_WithParameters_ReturnsListOfContas() {
        // Arrange
        String descricao = "";
        LocalDateTime dataVencimento = LocalDateTime.now();
        int pageNumber = 0;
        int pageSize = 10;
        List<Conta> contas = Collections.singletonList(new Conta());
        when(contaService.listContas(anyString(), any(LocalDateTime.class), anyInt(), anyInt())).thenReturn(contas);

        // Act
        ResponseEntity<List<Conta>> responseEntity = contaController.listContas(descricao, dataVencimento, pageNumber, pageSize);

        // Assert
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(contas, responseEntity.getBody());
    }

    @Test
    void getTotalValueInPeriod_ValidInput_ReturnsTotalValue() {
        // Arrange
        LocalDateTime dataInicio = LocalDateTime.now().minusDays(30);
        LocalDateTime dataFim = LocalDateTime.now();
        BigDecimal totalValue = BigDecimal.valueOf(1000);
        when(contaService.getTotalValueInPeriod(any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(totalValue);

        // Act
        ResponseEntity<BigDecimal> responseEntity = contaController.getTotalValueInPeriod(dataInicio, dataFim);

        // Assert
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(totalValue, responseEntity.getBody());
    }

    @Test
    void importFromCSV_ValidFile_ReturnsTotalCount() {
        // Arrange
        MultipartFile file = mock(MultipartFile.class);
        int totalCount = 10;
        when(contaService.importFromCSV(any(MultipartFile.class))).thenReturn(totalCount);

        // Act
        ResponseEntity<Integer> responseEntity = contaController.importFromCSV(file);

        // Assert
        verify(contaService, times(1)).importFromCSV(any());
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(totalCount, responseEntity.getBody());
    }

}
