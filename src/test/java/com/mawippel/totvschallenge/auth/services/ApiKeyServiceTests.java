package com.mawippel.totvschallenge.auth.services;

import com.mawippel.totvschallenge.domains.auth.entities.ApiKey;
import com.mawippel.totvschallenge.domains.auth.repositories.ApiKeyRepository;
import com.mawippel.totvschallenge.domains.auth.services.ApiKeyServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ApiKeyServiceTests {

    @Mock
    private ApiKeyRepository apiKeyRepository;

    @InjectMocks
    private ApiKeyServiceImpl apiKeyService;

    @Test
    void createKey_ValidApiKey_ReturnsApiKeyWithToken() {
        // Arrange
        ApiKey apiKey = new ApiKey();
        when(apiKeyRepository.save(any(ApiKey.class))).thenReturn(apiKey);

        // Act
        ApiKey createdApiKey = apiKeyService.createKey(apiKey);

        // Assert
        assertNotNull(createdApiKey);
        assertNotNull(createdApiKey.getToken());
    }

    @Test
    void getApiKey_ValidToken_ReturnsApiKey() {
        // Arrange
        String token = UUID.randomUUID().toString();
        ApiKey apiKey = new ApiKey();
        when(apiKeyRepository.getApiKeyByToken(any(UUID.class))).thenReturn(Optional.of(apiKey));

        // Act
        Optional<ApiKey> optionalApiKey = apiKeyService.getApiKey(token);

        // Assert
        assertTrue(optionalApiKey.isPresent());
        assertEquals(apiKey, optionalApiKey.get());
    }

    @Test
    void getApiKey_InvalidToken_ReturnsEmptyOptional() {
        // Arrange
        String token = UUID.randomUUID().toString();
        when(apiKeyRepository.getApiKeyByToken(any(UUID.class))).thenReturn(Optional.empty());

        // Act
        Optional<ApiKey> optionalApiKey = apiKeyService.getApiKey(token);

        // Assert
        assertTrue(optionalApiKey.isEmpty());
    }

}
