package com.mawippel.totvschallenge.auth.services;

import com.mawippel.totvschallenge.domains.auth.dtos.AuthenticationApiKeyDTO;
import com.mawippel.totvschallenge.domains.auth.entities.ApiKey;
import com.mawippel.totvschallenge.domains.auth.services.ApiKeyAuthenticationProviderService;
import com.mawippel.totvschallenge.domains.auth.services.ApiKeyService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthenticationToken;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ApiKeyAuthenticationProviderServiceTests {

    @Mock
    private ApiKeyService apiKeyService;

    @InjectMocks
    private ApiKeyAuthenticationProviderService authenticationProviderService;

    @Test
    void authenticate_ValidToken_ReturnsAuthenticationApiKeyDTO() {
        // Arrange
        String token = "validToken";
        ApiKey apiKey = ApiKey.builder()
                .email("foo@bar.com")
                .token(UUID.randomUUID())
                .createdAt(LocalDateTime.now())
                .build();
        when(apiKeyService.getApiKey(token)).thenReturn(Optional.of(apiKey));

        // Act
        Authentication authentication = authenticationProviderService.authenticate(new BearerTokenAuthenticationToken(token));

        // Assert
        assertNotNull(authentication);
        assertTrue(authentication instanceof AuthenticationApiKeyDTO);
    }

    @Test
    void authenticate_InvalidToken_ThrowsAuthenticationCredentialsNotFoundException() {
        // Arrange
        String token = "invalidToken";
        when(apiKeyService.getApiKey(token)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(AuthenticationCredentialsNotFoundException.class, () ->
                authenticationProviderService.authenticate(new BearerTokenAuthenticationToken(token)));
    }

}
