package com.mawippel.totvschallenge.auth.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mawippel.totvschallenge.domains.auth.controllers.ApiKeyController;
import com.mawippel.totvschallenge.domains.auth.entities.ApiKey;
import com.mawippel.totvschallenge.domains.auth.services.ApiKeyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
public class ApiKeyControllerTests {

    @Mock
    private ApiKeyService apiKeyService;

    @InjectMocks
    private ApiKeyController apiKeyController;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(apiKeyController).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void createApiKey_ValidApiKey_ReturnsTokenResponse() throws Exception {
        // Arrange
        ApiKey apiKey = ApiKey.builder()
                .email("foo@bar.com")
                .token(UUID.randomUUID())
                .build();

        when(apiKeyService.createKey(any(ApiKey.class))).thenReturn(apiKey);

        // Act & Assert
        mockMvc.perform(post("/api-keys")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(apiKey)))
                .andExpect(status().isCreated());
    }
}
