CREATE TABLE IF NOT EXISTS conta
(
    id                SERIAL PRIMARY KEY,
    descricao         VARCHAR(255) NOT NULL,
    valor             NUMERIC(30,2) NOT NULL,
    situacao          VARCHAR(32),
    data_vencimento   TIMESTAMP NOT NULL,
    data_pagamento    TIMESTAMP,
    created_by        VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS api_key
(
    id           SERIAL PRIMARY KEY,
    email        VARCHAR(255) NOT NULL,
    token        UUID NOT NULL,
    created_at   TIMESTAMP NOT NULL
);