package com.mawippel.totvschallenge.domains.conta.entities;

import com.mawippel.totvschallenge.domains.conta.shared.enums.SituacaoConta;
import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class Conta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column
    @NotBlank(message = "descrição nao pode ser null ou vazia")
    private String descricao;

    @Column
    @DecimalMin(value = "0", message = "valor deve ser maior que zero")
    private BigDecimal valor;

    @Enumerated(EnumType.STRING)
    private SituacaoConta situacao = SituacaoConta.PENDENTE;

    @Column(name="data_vencimento")
    @NotNull(message = "data de vencimento nao pode ser null")
    private LocalDateTime dataVencimento;

    @Column(name="data_pagamento")
    private LocalDateTime dataPagamento;

    @CreatedBy
    @Column(name="created_by")
    private String createdBy;
}
