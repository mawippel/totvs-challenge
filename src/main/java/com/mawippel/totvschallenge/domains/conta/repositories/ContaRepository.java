package com.mawippel.totvschallenge.domains.conta.repositories;

import com.mawippel.totvschallenge.domains.conta.entities.Conta;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.ListPagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.stream.Stream;

@Repository
public interface ContaRepository extends JpaRepository<Conta, Long>, ListPagingAndSortingRepository<Conta, Long> {

    Stream<Conta> getContasByDescricaoContainingIgnoreCaseAndDataVencimentoIsBefore(String descricao, LocalDateTime dataVencimento, Pageable pageable);

    @Query("SELECT SUM(conta.valor) FROM Conta conta WHERE conta.dataPagamento >= ?1 and conta.dataPagamento < ?2")
    BigDecimal getTotalValueBetweenDataInicioAndDataFim(LocalDateTime dataInicio, LocalDateTime dataFim);
}
