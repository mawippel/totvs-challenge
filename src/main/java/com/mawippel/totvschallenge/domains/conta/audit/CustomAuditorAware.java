package com.mawippel.totvschallenge.domains.conta.audit;

import com.mawippel.totvschallenge.domains.auth.dtos.AuthenticationApiKeyDTO;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class CustomAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }
        var loggedUser = (AuthenticationApiKeyDTO) authentication;
        return Optional.of(loggedUser.getEmail());
    }
}
