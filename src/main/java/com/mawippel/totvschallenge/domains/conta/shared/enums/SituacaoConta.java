package com.mawippel.totvschallenge.domains.conta.shared.enums;

public enum SituacaoConta {
    PENDENTE,
    PAGA,
    ATRASADA
}
