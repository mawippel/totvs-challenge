package com.mawippel.totvschallenge.domains.conta.controllers;

import com.mawippel.totvschallenge.domains.conta.entities.Conta;
import com.mawippel.totvschallenge.domains.conta.services.ContaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("contas")
@RequiredArgsConstructor
@Slf4j
public class ContaController {

    private final ContaService contaService;

    @PostMapping
    public ResponseEntity<Conta> createConta(@RequestBody Conta conta) {
        return ResponseEntity.status(HttpStatus.CREATED).body(contaService.createConta(conta));
    }

    @PutMapping("{id}")
    public ResponseEntity<Conta> updateConta(@PathVariable Long id, @RequestBody Conta conta) {
        return ResponseEntity.ok(contaService.updateConta(id, conta));
    }

    @PatchMapping("{id}")
    public ResponseEntity<Conta> updateSituacaoConta(@PathVariable Long id, @RequestBody Conta conta) {
        return ResponseEntity.ok(contaService.updateSituacaoConta(id, conta.getSituacao()));
    }

    @GetMapping("{id}")
    public ResponseEntity<Conta> getContaById(@PathVariable Long id) {
        return ResponseEntity.ok(contaService.getContaById(id));
    }

    @GetMapping
    public ResponseEntity<List<Conta>> listContas(@RequestParam(value = "descricao", required = false, defaultValue = "") String descricao,
                                                   @RequestParam("data_vencimento") LocalDateTime dataVencimento,
                                                   @RequestParam(value = "page_number", required = false) Integer pageNumber,
                                                   @RequestParam(value = "page_size", required = false) Integer pageSize) {
        // valores default para pageNumber e pageSize
        pageNumber = pageNumber == null || pageNumber < 0 ? 0 : pageNumber;
        pageSize = pageSize == null || pageSize <= 0 ? 10 : pageSize;
        return ResponseEntity.ok(contaService.listContas(descricao, dataVencimento, pageNumber, pageSize));
    }

    @GetMapping("/totais:noPeriodo")
    public ResponseEntity<BigDecimal> getTotalValueInPeriod(@RequestParam("data_inicio") LocalDateTime dataInicio,
                                                             @RequestParam("data_fim") LocalDateTime dataFim) {
        BigDecimal totalValueInPeriod = contaService.getTotalValueInPeriod(dataInicio, dataFim);
        totalValueInPeriod = totalValueInPeriod == null ? BigDecimal.valueOf(0) : totalValueInPeriod;
        return ResponseEntity.ok(totalValueInPeriod);
    }

    @PostMapping("/uploads")
    public ResponseEntity<Integer> importFromCSV(@RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok(contaService.importFromCSV(file));
    }
}
