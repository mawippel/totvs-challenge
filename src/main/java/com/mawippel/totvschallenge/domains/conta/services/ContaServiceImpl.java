package com.mawippel.totvschallenge.domains.conta.services;

import com.google.common.collect.Iterators;
import com.mawippel.totvschallenge.domains.conta.entities.Conta;
import com.mawippel.totvschallenge.domains.conta.exceptions.ContaNotFoundException;
import com.mawippel.totvschallenge.domains.conta.repositories.ContaRepository;
import com.mawippel.totvschallenge.domains.conta.shared.enums.SituacaoConta;
import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.mawippel.totvschallenge.domains.conta.shared.constants.CSVHeaderConstants.*;

@Service
@RequiredArgsConstructor
public class ContaServiceImpl implements ContaService {

    private final ContaRepository contaRepository;

    private static final int BATCH_SIZE = 50;

    @Override
    public Conta createConta(Conta conta) {
        return contaRepository.save(conta);
    }

    @Override
    public Conta updateConta(Long id, Conta conta) {
        Conta contaFromDb = contaRepository.findById(id).orElseThrow(ContaNotFoundException::new);
        contaFromDb.setValor(conta.getValor());
        contaFromDb.setDescricao(conta.getDescricao());
        contaFromDb.setSituacao(conta.getSituacao());
        contaFromDb.setDataVencimento(conta.getDataVencimento());
        contaFromDb.setDataPagamento(conta.getDataPagamento());
        return contaRepository.saveAndFlush(contaFromDb);
    }

    @Override
    public Conta updateSituacaoConta(Long id, SituacaoConta situacaoConta) {
        if (situacaoConta == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "situacao invalida");
        }

        Conta contaFromDb = contaRepository.findById(id).orElseThrow(ContaNotFoundException::new);

        if (SituacaoConta.PAGA.equals(situacaoConta)) {
            contaFromDb.setDataPagamento(LocalDateTime.now(ZoneOffset.UTC));
        }

        contaFromDb.setSituacao(situacaoConta);

        return contaRepository.saveAndFlush(contaFromDb);
    }

    @Override
    public Conta getContaById(Long id) {
        return contaRepository.findById(id).orElseThrow(ContaNotFoundException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Conta> listContas(String descricao, LocalDateTime dataVencimento, int pageNumber, int pageSize) {
        Pageable sortedByPriceDescNameAsc = PageRequest.of(pageNumber, pageSize, Sort.by("id").ascending());
        return contaRepository.getContasByDescricaoContainingIgnoreCaseAndDataVencimentoIsBefore(descricao, dataVencimento, sortedByPriceDescNameAsc)
                .toList();
    }

    @Override
    public BigDecimal getTotalValueInPeriod(LocalDateTime dataInicio, LocalDateTime dataFim) {
        return contaRepository.getTotalValueBetweenDataInicioAndDataFim(dataInicio, dataFim);
    }

    @Override
    @Transactional
    public Integer importFromCSV(MultipartFile file) {
        AtomicInteger counter = new AtomicInteger();
        CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                .setHeader(VALOR, DESCRICAO, SITUACAO, DATA_PAGAMENTO, DATA_VENCIMENTO)
                .setSkipHeaderRecord(true)
                .build();
        try {
            CSVParser parse = csvFormat.parse(new InputStreamReader(file.getInputStream()));
            Iterators.partition(parse.iterator(), BATCH_SIZE).forEachRemaining(linesList -> {
                List<Conta> list = linesList.stream().map(line -> Conta.builder()
                        .valor(new BigDecimal(line.get(VALOR)))
                        .situacao(parseSituacaoConta(line.get(SITUACAO)))
                        .descricao(line.get(DESCRICAO))
                        .dataPagamento(parseLocalDateTime(line.get(DATA_PAGAMENTO)))
                        .dataVencimento(parseLocalDateTime(line.get(DATA_VENCIMENTO)))
                        .build()
                ).toList();
                contaRepository.saveAll(list);
                counter.addAndGet(list.size());
            });
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "arquivo invalido");
        }

        return counter.get();
    }

    private SituacaoConta parseSituacaoConta(String situacaoConta) {
        if (situacaoConta.isBlank()) {
            return SituacaoConta.PENDENTE;
        }
        return SituacaoConta.valueOf(situacaoConta);
    }

    LocalDateTime parseLocalDateTime(String value) {
        if (value.isBlank()) {
            return null;
        }
        return LocalDateTime.parse(value, DateTimeFormatter.ISO_DATE_TIME);
    }

}
