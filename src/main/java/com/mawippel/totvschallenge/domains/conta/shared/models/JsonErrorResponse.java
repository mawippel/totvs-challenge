package com.mawippel.totvschallenge.domains.conta.shared.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class JsonErrorResponse {

    private List<String> errors;
}
