package com.mawippel.totvschallenge.domains.conta.services;

import com.mawippel.totvschallenge.domains.conta.entities.Conta;
import com.mawippel.totvschallenge.domains.conta.shared.enums.SituacaoConta;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface ContaService {

    Conta createConta(Conta conta);

    Conta updateConta(Long id, Conta conta);

    Conta updateSituacaoConta(Long id, SituacaoConta situacaoConta);

    Conta getContaById(Long id);

    List<Conta> listContas(String descricao, LocalDateTime dataVencimento, int pageNumber, int pageSize);

    BigDecimal getTotalValueInPeriod(LocalDateTime dataInicio, LocalDateTime dataFim);

    Integer importFromCSV(MultipartFile file);
}
