package com.mawippel.totvschallenge.domains.conta.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ContaNotFoundException extends RuntimeException {
}
