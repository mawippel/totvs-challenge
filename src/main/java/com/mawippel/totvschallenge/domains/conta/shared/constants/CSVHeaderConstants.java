package com.mawippel.totvschallenge.domains.conta.shared.constants;

public interface CSVHeaderConstants {

    String VALOR = "VALOR";
    String DESCRICAO = "DESCRICAO";
    String SITUACAO = "SITUACAO";
    String DATA_PAGAMENTO = "DATA_PAGAMENTO";
    String DATA_VENCIMENTO = "DATA_VENCIMENTO";
}
