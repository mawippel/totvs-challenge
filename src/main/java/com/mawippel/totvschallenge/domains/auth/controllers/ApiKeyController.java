package com.mawippel.totvschallenge.domains.auth.controllers;

import com.mawippel.totvschallenge.domains.auth.dtos.TokenResponseDTO;
import com.mawippel.totvschallenge.domains.auth.entities.ApiKey;
import com.mawippel.totvschallenge.domains.auth.services.ApiKeyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api-keys")
@RequiredArgsConstructor
@Slf4j
public class ApiKeyController {

    private final ApiKeyService apiKeyService;

    @PostMapping
    private ResponseEntity<TokenResponseDTO> createApiKey(@RequestBody ApiKey apiKey) {
        ApiKey key = apiKeyService.createKey(apiKey);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(TokenResponseDTO.builder().token(key.getToken().toString()).build());
    }

}
