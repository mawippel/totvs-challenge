package com.mawippel.totvschallenge.domains.auth.services;

import com.mawippel.totvschallenge.domains.auth.entities.ApiKey;

import java.util.Optional;

public interface ApiKeyService {

    ApiKey createKey(ApiKey apiKey);

    Optional<ApiKey> getApiKey(String token);
}
