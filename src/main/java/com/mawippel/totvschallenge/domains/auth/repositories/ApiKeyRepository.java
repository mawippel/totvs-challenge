package com.mawippel.totvschallenge.domains.auth.repositories;

import com.mawippel.totvschallenge.domains.auth.entities.ApiKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ApiKeyRepository extends JpaRepository<ApiKey, Long> {

    Optional<ApiKey> getApiKeyByToken(UUID token);
}
