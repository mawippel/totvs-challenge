package com.mawippel.totvschallenge.domains.auth.dtos;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TokenResponseDTO {

    private final String token;
}
