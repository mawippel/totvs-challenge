package com.mawippel.totvschallenge.domains.auth.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationApiKeyDTO implements Authentication {

    private String email;
    private String token;
    private Instant expiration;
    private Set<GrantedAuthority> authorities;
    // Esse campo nao eh utilizado, apenas para suporte futuro
    private List<String> permissions;

    @Override
    public boolean isAuthenticated() {
        boolean isAuthenticated = LocalDateTime.now().toInstant(ZoneOffset.UTC).isBefore(getExpiration());
        if (Boolean.FALSE.equals(isAuthenticated)) {
            throw new CredentialsExpiredException("token expirado");
        }
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        throw new IllegalArgumentException();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getName() {
        return email;
    }

    @Override
    public Object getCredentials() {
        return String.join(email, ":", token);
    }

    @Override
    public Object getPrincipal() {
        return String.join(email, ":", token);
    }

    @Override
    public Object getDetails() {
        return this.toString();
    }

}
