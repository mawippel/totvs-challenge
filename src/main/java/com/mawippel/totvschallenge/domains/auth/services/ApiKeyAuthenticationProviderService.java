package com.mawippel.totvschallenge.domains.auth.services;

import com.mawippel.totvschallenge.domains.auth.dtos.AuthenticationApiKeyDTO;
import com.mawippel.totvschallenge.domains.auth.entities.ApiKey;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthenticationToken;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ApiKeyAuthenticationProviderService implements AuthenticationProvider {

    private final ApiKeyService apiKeyService;

    private int tokenExpirationMinutes;

    @Value("${auth.token.expiration.in.minutes}")
    public void setTokenTopicName(int tokenExpirationMinutes) {
        this.tokenExpirationMinutes = tokenExpirationMinutes;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        var bearerToken = (BearerTokenAuthenticationToken) authentication;
        var token = bearerToken.getToken();
        return apiKeyService.getApiKey(token)
                .map(this::buildAuthentication)
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("token nao encontrado"));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return BearerTokenAuthenticationToken.class.isAssignableFrom(authentication);
    }

    private AuthenticationApiKeyDTO buildAuthentication(ApiKey apiKey) {
        return AuthenticationApiKeyDTO.builder()
                .email(apiKey.getEmail())
                .token(apiKey.getToken().toString())
                // seta o expiration de acordo com a data de criacao do token
                .expiration(apiKey.getCreatedAt().plusMinutes(tokenExpirationMinutes).toInstant(ZoneOffset.UTC))
                // vazio por enquanto, este campo seria populado com permissoes reais
                .permissions(List.of())
                // numa aplicacao real, seria feito uma chamada para um AuthoritiesBuilderService para gerar as GAs
                .authorities(Set.of())
                .build();
    }
}
