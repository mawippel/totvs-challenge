package com.mawippel.totvschallenge.domains.auth.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "api_key", indexes = {
        @Index(columnList = "token")
})
public class ApiKey {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column
    @Email(message = "email invalido")
    private String email;

    @Column
    private UUID token;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;
}


