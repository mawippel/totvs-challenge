package com.mawippel.totvschallenge.domains.auth.services;

import com.mawippel.totvschallenge.domains.auth.entities.ApiKey;
import com.mawippel.totvschallenge.domains.auth.repositories.ApiKeyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ApiKeyServiceImpl implements ApiKeyService {

    private final ApiKeyRepository apiKeyRepository;

    @Override
    public ApiKey createKey(ApiKey apiKey) {
        apiKey.setToken(UUID.randomUUID());
        return apiKeyRepository.save(apiKey);
    }

    @Override
    public Optional<ApiKey> getApiKey(String token) {
        return apiKeyRepository.getApiKeyByToken(UUID.fromString(token));
    }
}
